import UIKit

protocol StartOverViewControllerDelegate: class {
	func startOverControllerWantsToOpenVideoPicker(_ startOverController: StartOverViewController)
}

class StartOverViewController: UIViewController {
	weak var delegate: StartOverViewControllerDelegate?

	@IBAction
	func openVideoPicker() {
		delegate?.startOverControllerWantsToOpenVideoPicker(self)
	}
}
