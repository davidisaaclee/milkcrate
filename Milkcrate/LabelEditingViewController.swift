import UIKit
import MobileCoreServices

protocol LabelEditingViewControllerDelegate: class {
	func labelEditor(_ labelEditor: LabelEditingViewController, didFinishWith image: UIImage, labelMedia: LabelMedia)
}

protocol LabelEditingViewControllerDataSource: class {
	func labelMediaForLabelEditor(_ labelEditor: LabelEditingViewController) -> LabelMedia
}

class LabelEditingViewController: UIViewController {

	weak var delegate: LabelEditingViewControllerDelegate?
	weak var dataSource: LabelEditingViewControllerDataSource? {
		didSet {
			updateLabelMediaFromDataSource()
		}
	}
	
	let exportImageSize = CGSize(width: 500, height: 500)
	
	var labelMedia: LabelMedia = LabelMedia(image: nil, text: nil, textColor: .black) {
		didSet {
			displayLabelMedia(labelMedia)
		}
	}

	@IBOutlet
	weak var albumArtworkContainerView: UIView?

	@IBOutlet
	weak var textView: UITextView?
	
	@IBOutlet
	weak var imageView: UIImageView?

	override func viewDidLoad() {
		super.viewDidLoad()
		displayLabelMedia(labelMedia)
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		textView?.becomeFirstResponder()
	}

	@IBAction
	func pickImage() {
		let imagePicker = UIImagePickerController()
		imagePicker.mediaTypes = [kUTTypeImage as String]
		imagePicker.allowsEditing = true
		imagePicker.delegate = self
		present(imagePicker, animated: true, completion: nil)
	}
	
	@IBAction
	func removeImage() {
		labelMedia.image = nil
	}

	@IBAction
	func handleLightDarkSelection(from segmentedControl: UISegmentedControl) {
		switch segmentedControl.selectedSegmentIndex {
		case 0:
			labelMedia.textColor = .black

		case 1:
			labelMedia.textColor = .white

		default:
			fatalError("Implement me")
		}
	}
	
	@IBAction
	func finish() {
		guard let albumArtworkContainerView = albumArtworkContainerView else {
			return
		}
		
		let poppedTint = textView?.tintColor
		textView?.tintColor = .clear
		
		UIGraphicsBeginImageContextWithOptions(
			albumArtworkContainerView.bounds.integral.size,
			true,
			0)
			// UIScreen.main.scale)
		albumArtworkContainerView.drawHierarchy(in: albumArtworkContainerView.bounds.integral,
		                                        afterScreenUpdates: true)
		let image = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		
		textView?.tintColor = poppedTint
		
		delegate?.labelEditor(self, didFinishWith: image, labelMedia: labelMedia)
	}


	func updateLabelMediaFromDataSource() {
		guard let dataSource = dataSource else {
			return
		}

		labelMedia = dataSource.labelMediaForLabelEditor(self)
	}

	func displayLabelMedia(_ labelMedia: LabelMedia) {
		if let imageView = imageView {
			imageView.image = labelMedia.image
		}
		if let textView = textView {
			textView.textColor = labelMedia.textColor
			textView.text = labelMedia.text
		}
	}
}

extension LabelEditingViewController: UINavigationControllerDelegate {}
extension LabelEditingViewController: UIImagePickerControllerDelegate {
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true, completion: nil)
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
			return
		}

		labelMedia.image = image.resizing(toFitWithin: exportImageSize)
		picker.dismiss(animated: true, completion: nil)
	}
}

extension LabelEditingViewController: UITextViewDelegate {
	func textViewDidChange(_ textView: UITextView) {
		labelMedia.text = textView.text
	}
}

