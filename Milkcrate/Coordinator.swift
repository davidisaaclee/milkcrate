import Foundation
import UIKit
import Photos
import MobileCoreServices
import AVFoundation

extension CGSize {
	var floored: CGSize {
		return CGSize(width: floor(width), height: floor(height))
	}
}

enum Result<T> {
	case success(T)
	case failure(Error)
}

struct LabelMedia {
	static let blank = LabelMedia(image: nil, text: nil, textColor: .black)

	var image: UIImage?
	var text: String?

	var textColor: UIColor = .black

	var isEmpty: Bool {
		return image == nil && text == nil
	}

	func render(preferredSize: CGSize) -> UIImage {
		let imageBounds = CGRect(origin: .zero, size: image?.size ?? preferredSize)

		UIGraphicsBeginImageContext(imageBounds.size)

		UIColor.white.set()
		UIBezierPath(rect: imageBounds).fill()

		if let image = image {
			image.draw(in: imageBounds)
		}

		if let text = text {
			let nsText = NSString(string: text)

			let font = UIFont.boldSystemFont(ofSize: 100)
			let paragraphStyle = NSMutableParagraphStyle()
			paragraphStyle.alignment = .left

			nsText.draw(with: imageBounds, options: [.usesLineFragmentOrigin], attributes: [
				.font: font,
				.paragraphStyle: paragraphStyle,
				.foregroundColor: textColor
			], context: nil)
		}

		let resultImage = UIGraphicsGetImageFromCurrentImageContext()!

		UIGraphicsEndImageContext()

		return resultImage
	}
}

class Coordinator: NSObject {
	struct State {
		var videoAsset: AVAsset?
		var albumArtwork: UIImage?
		var labelMedia: LabelMedia

		static let empty = State(videoAsset: nil, albumArtwork: nil, labelMedia: .blank)
	}

	var state: State = .empty

	let navigationController: UINavigationController

	init(navigationController: UINavigationController = UINavigationController()) {
		self.navigationController = navigationController
	}

	func setup() {
		navigationController.isNavigationBarHidden = true
		navigationController.viewControllers = [makeComposeViewController()]
	}

	func milkcratify(updateProgress: @escaping (Float) -> Void, completion: ((Result<URL>) -> Void)?) {
		guard let videoAsset = state.videoAsset, let image = state.albumArtwork else {
			return
		}
		
		let fullDuration = CMTimeRange(start: kCMTimeZero, duration: videoAsset.duration)

		var animationTrackID: CMPersistentTrackID!
		var animationTrack: AVAssetTrack!

		func makeComposition() -> AVComposition {
			let comp = AVMutableComposition()

			do {
				try comp.insertTimeRange(fullDuration, of: videoAsset, at: kCMTimeZero)

				animationTrack = comp.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)!
				animationTrackID = animationTrack.trackID
			} catch {
				fatalError("\(error)")
			}

			return comp
		}

		func makeVideoComposition(for asset: AVAsset) -> AVVideoComposition {
			let videoComp = AVMutableVideoComposition()
			videoComp.renderSize = image.size.floored
			// Since we have a single "frame", the frame duration is the entire length of the asset.
			videoComp.frameDuration = videoAsset.duration
			videoComp.renderScale = 1
			
			let parentLayer = CALayer()
			parentLayer.contents = image.cgImage
			parentLayer.frame = CGRect(origin: .zero, size: videoComp.renderSize)

			parentLayer.rasterizationScale = 2
			parentLayer.contentsScale = 2
			
			videoComp.animationTool = AVVideoCompositionCoreAnimationTool(additionalLayer: parentLayer, asTrackID: animationTrackID)

			let showAnimInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: animationTrack)
			let instruction = AVMutableVideoCompositionInstruction()
			instruction.layerInstructions = [showAnimInstruction]
			instruction.timeRange = fullDuration

			videoComp.instructions = [instruction]
			
			return videoComp
		}

		let composition = makeComposition()
		let videoComposition = makeVideoComposition(for: composition)

		export(composition, videoComposition: videoComposition, updateProgress: updateProgress, completion: completion)
	}

	func export(_ composition: AVComposition, videoComposition: AVVideoComposition, updateProgress: ((Float) -> Void)?, completion: ((Result<URL>) -> Void)?) {
		guard let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality) else {
			return
		}

		let temporaryURL =
			FileManager.default.temporaryDirectory.appendingPathComponent("\(UUID().uuidString).mov")

		exporter.videoComposition = videoComposition
		exporter.outputURL = temporaryURL
		exporter.outputFileType = .mov
		exporter.shouldOptimizeForNetworkUse = true

		var progressUpdateTimer: Timer?
		if let updateProgress = updateProgress {
			progressUpdateTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
				updateProgress(exporter.progress)
			}
		}

		exporter.exportAsynchronously {
			progressUpdateTimer?.invalidate()
			if let error = exporter.error {
				completion?(.failure(error))
			} else {
				completion?(.success(temporaryURL))
			}
		}
	}
}


// MARK: Making view controllers

extension Coordinator {
	func makeComposeViewController() -> ComposeViewController {
		let controller = ComposeViewController()
		controller.delegate = self
		controller.dataSource = self
		return controller
	}
}

extension Coordinator: ComposeViewControllerDelegate {
	func composeViewController(_ composeViewController: ComposeViewController, didRenderAlbumArtwork image: UIImage) {
		state.albumArtwork = image
	}
	
	func composeViewController(_ composeViewController: ComposeViewController, didSetCoverBackgroundImage image: UIImage?) {
		state.labelMedia.image = image
	}
	
	func composeViewController(_ composeViewController: ComposeViewController, didSetSoundtrackVideo soundtrackVideo: AVAsset) {
		state.videoAsset = soundtrackVideo
	}
	
	func composeViewController(_ composeViewController: ComposeViewController, didSetCoverText text: String) {
		state.labelMedia.text = text
	}

	func composeViewController(_ composeViewController: ComposeViewController,
	                           didSetCoverTextColor textColor: UIColor) {
		state.labelMedia.textColor = textColor
	}
	
	func composeViewControllerWantsToSave(_ composeViewController: ComposeViewController) {
		func shareVideo(url: URL, completion: ((Bool) -> Void)?) {
			let activityController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
			activityController.completionWithItemsHandler = { (_, completed, _, _) in
				completion?(completed)
			}
			self.navigationController.present(activityController, animated: true)
		}
		
		milkcratify(updateProgress: { progress in	}, completion: { (urlResult) in
			switch urlResult {
			case let .failure(error):
				print("ERROR: \(error)")
				
			case let .success(videoURL):
				DispatchQueue.main.async {
					shareVideo(url: videoURL, completion: { completed in
						if completed {
							self.navigationController.popToRootViewController(animated: true)
						}
					})
				}
			}
		})
	}
	
	func composeViewControllerWantsToReset(_ composeViewController: ComposeViewController) {
		state = .empty
	}
	
	func composeViewControllerShouldEnableExport(_ composeViewController: ComposeViewController) -> Bool {
		return state.videoAsset != nil
	}
}

extension Coordinator: ComposeViewControllerDataSource {
	func coverTextForComposeViewController(_ composeViewController: ComposeViewController) -> String? {
		return state.labelMedia.text
	}
	
	func soundtrackAssetForComposeViewController(_ composeViewController: ComposeViewController) -> AVAsset? {
		return state.videoAsset
	}
	
	func coverBackgroundImageForComposeViewController(_ composeViewController: ComposeViewController) -> UIImage? {
		return state.labelMedia.image
	}
	
	func coverTextColorForComposeViewController(_ composeViewController: ComposeViewController) -> UIColor {
		return state.labelMedia.textColor
	}
}

