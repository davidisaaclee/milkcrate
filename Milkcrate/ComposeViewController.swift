import UIKit
import AVFoundation
import MobileCoreServices

extension AVPlayer {
	func stop() {
		pause()
		seek(to: kCMTimeZero)
	}
}

protocol ComposeViewControllerDelegate: class {
	func composeViewController(_ composeViewController: ComposeViewController,
	                           didSetSoundtrackVideo soundtrackVideo: AVAsset)
	func composeViewController(_ composeViewController: ComposeViewController,
	                           didSetCoverText text: String)
	func composeViewController(_ composeViewController: ComposeViewController,
	                           didSetCoverTextColor textColor: UIColor)
	func composeViewController(_ composeViewController: ComposeViewController,
	                           didSetCoverBackgroundImage image: UIImage?)
	func composeViewController(_ composeViewController: ComposeViewController,
	                           didRenderAlbumArtwork image: UIImage)
	
	func composeViewControllerShouldEnableExport(_ composeViewController: ComposeViewController) -> Bool
	
	func composeViewControllerWantsToSave(_ composeViewController: ComposeViewController)
	func composeViewControllerWantsToReset(_ composeViewController: ComposeViewController)
}

protocol ComposeViewControllerDataSource: class {
	func coverBackgroundImageForComposeViewController(_ composeViewController: ComposeViewController) -> UIImage?
	func coverTextForComposeViewController(_ composeViewController: ComposeViewController) -> String?
	func coverTextColorForComposeViewController(_ composeViewController: ComposeViewController) -> UIColor
	func soundtrackAssetForComposeViewController(_ composeViewController: ComposeViewController) -> AVAsset?
}

class ComposeViewController: UIViewController {

	weak var delegate: ComposeViewControllerDelegate?
	weak var dataSource: ComposeViewControllerDataSource? {
		didSet {
			reloadData()
		}
	}

	@IBOutlet
	weak var coverBackgroundImageView: UIImageView?

	@IBOutlet
	weak var coverTextView: UITextView?

	@IBOutlet
	weak var albumArtworkContainerView: UIView?

	@IBOutlet
	weak var playbarProgressView: UISlider?
	
	@IBOutlet
	weak var playButton: UIButton?
	
	@IBOutlet
	weak var chooseSoundtrackButton: UIButton?
	
	@IBOutlet
	weak var loadedSoundtrackContainerView: UIView?
	
	@IBOutlet
	weak var exportButton: UIButton?
//		{
//		didSet {
//			if let exportButton = exportButton {
//				let title =
//					NSMutableAttributedString(string: "Export",
//					                          attributes: [.font: UIFont.systemFont(ofSize: 32, weight: .bold),
//					                                       .foregroundColor: UIColor.darkGray])
//
//				let strikethroughTitle = title.mutableCopy() as! NSMutableAttributedString
//				strikethroughTitle.addAttribute(.strikethroughStyle,
//				                                value: NSUnderlineStyle.styleThick.rawValue,
//				                                range: NSRange(location: 0,
//				                                               length: strikethroughTitle.length))
//
//				exportButton.setAttributedTitle(title, for: .normal)
//				exportButton.setAttributedTitle(strikethroughTitle, for: .disabled)
//			}
//		}
//	}
	
	@IBOutlet
	weak var helpView: UIView?
	
	let player = AVPlayer()
	
	fileprivate weak var coverBackgroundImagePicker: UIImagePickerController?
	fileprivate weak var soundtrackVideoPicker: UIImagePickerController?

	fileprivate var playerObserverToken: Any?
	fileprivate var isScrubbing: Bool = false
	
	fileprivate var isSoundtrackLoaded: Bool = false {
		didSet {
			chooseSoundtrackButton?.isHidden = isSoundtrackLoaded
			loadedSoundtrackContainerView?.isHidden = !isSoundtrackLoaded
		}
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		playerObserverToken =
			player.addPeriodicTimeObserver(forInterval: CMTime(value: 1, timescale: 30), queue: .main) { [weak self] (time) in
				self?.updatePlaybackProgress(time: time)
			}
		
		updatePlaybackProgress(time: kCMTimeZero)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		reloadData()
	}
	
	func play() {
		player.play()
		playButton?.isSelected = true
	}
	
	func stop() {
		player.stop()
		playButton?.isSelected = false
	}

	@IBAction
	func pickSoundtrackVideo() {
		// Stop playback before displaying picker.
		stop()
		
		let videoPicker = UIImagePickerController()
		videoPicker.mediaTypes = [kUTTypeMovie as String]
		videoPicker.allowsEditing = true
		videoPicker.delegate = self

		soundtrackVideoPicker = videoPicker
		present(videoPicker, animated: true)
	}
	
	@IBAction
	func pickCoverBackgroundImage() {
		let imagePicker = UIImagePickerController()
		imagePicker.mediaTypes = [kUTTypeImage as String]
		imagePicker.allowsEditing = true
		imagePicker.delegate = self

		coverBackgroundImagePicker = imagePicker
		present(imagePicker, animated: true)
	}
	
	@IBAction
	func focusText() {
		coverTextView?.becomeFirstResponder()
	}
	
	@IBAction
	func clearCoverBackgroundImage() {
		delegate?.composeViewController(self, didSetCoverBackgroundImage: nil)
		reloadData()
	}
	
	@IBAction
	func playSoundtrack() {
		if player.rate > 0 {
			stop()
		} else {
			play()
		}
	}
	
	@IBAction
	func save() {
		delegate?.composeViewController(self, didRenderAlbumArtwork: renderAlbumArtwork())
		
		delegate?.composeViewControllerWantsToSave(self)
	}
	
	@IBAction
	func reset() {
		delegate?.composeViewControllerWantsToReset(self)
		reloadData()
	}
	
	@IBAction
	private func handleBackgroundImageModification(from segmentControl: UISegmentedControl) {
		switch segmentControl.selectedSegmentIndex {
		case 0:
			pickCoverBackgroundImage()
		case 1:
			clearCoverBackgroundImage()
		default:
			break
		}
	}

	@IBAction
	func changeCoverText(from textView: UITextView) {
		delegate?.composeViewController(self, didSetCoverText: textView.text ?? "")
	}
	
	func changeCoverText(to text: String) {
		delegate?.composeViewController(self, didSetCoverText: text)
	}
	
	private func reloadData() {
		coverBackgroundImageView?.image = dataSource?.coverBackgroundImageForComposeViewController(self)
		coverTextView?.text = dataSource?.coverTextForComposeViewController(self)
		coverTextView?.textColor = dataSource?.coverTextColorForComposeViewController(self)
		
		// Change background color based on text color if no cover background image.
		if let textColor = coverTextView?.textColor, coverBackgroundImageView?.image == nil {
			switch textColor {
			case .white:
				coverBackgroundImageView?.backgroundColor = .black
				
			case .black:
				coverBackgroundImageView?.backgroundColor = .white
				
			default:
				break
			}
		}
		
		let soundtrackAssetOrNil = dataSource?.soundtrackAssetForComposeViewController(self)
		isSoundtrackLoaded = soundtrackAssetOrNil != nil
		let playerItemOrNil = soundtrackAssetOrNil.map { AVPlayerItem(asset: $0) }
		player.replaceCurrentItem(with: playerItemOrNil)
		
		NotificationCenter.default.removeObserver(self)
		
		if let playerItem = playerItemOrNil {
			NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: playerItem, queue: nil) { [weak self] (notification) in
				self?.stop()
			}
		}
		
		exportButton?.isEnabled = delegate?.composeViewControllerShouldEnableExport(self) ?? false
	}
	
	func renderAlbumArtwork() -> UIImage {
		guard let albumArtworkContainerView = albumArtworkContainerView else {
			fatalError("Implement me")
		}

		let poppedTint = coverTextView?.tintColor
		coverTextView?.tintColor = .clear
		
		UIGraphicsBeginImageContextWithOptions(
			albumArtworkContainerView.bounds.integral.size,
			true,
			0)
		albumArtworkContainerView.drawHierarchy(in: albumArtworkContainerView.bounds.integral,
		                                        afterScreenUpdates: true)
		let image = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		
		coverTextView?.tintColor = poppedTint

		return image
	}
	
	@IBAction
 	func scrubDidBegin() {
		isScrubbing = true
	}
	
	@IBAction
	func scrubDidFinish(from slider: UISlider) {
		guard let itemDuration = player.currentItem?.duration else {
			return
		}
		
		let timeToScrubTo = TimeInterval(slider.value) * itemDuration.seconds
		player.seek(to: CMTime(seconds: timeToScrubTo, preferredTimescale: 400)) { (completed) in
			self.isScrubbing = false
		}
	}

	@IBAction
	func showHelp() {
		helpView?.isHidden = false
	}
	
	@IBAction
	func hideHelp() {
		helpView?.isHidden = true
	}
	
	@objc
	private func setLightText() {
		delegate?.composeViewController(self, didSetCoverTextColor: .white)
		reloadData()
	}
	
	@objc
	private func setDarkText() {
		delegate?.composeViewController(self, didSetCoverTextColor: .black)
		reloadData()
	}
	
	private func updatePlaybackProgress(time: CMTime) {
		guard let itemDuration = self.player.currentItem?.duration else {
			self.playbarProgressView?.value = 0
			return
		}
		
		// If user is scrubbing, don't update progress view.
		guard !isScrubbing else {
			return
		}
		
		let progress = time.seconds / itemDuration.seconds
		self.playbarProgressView?.value = Float(progress)
	}
	
}

extension ComposeViewController: UINavigationControllerDelegate {}
extension ComposeViewController: UIImagePickerControllerDelegate {
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		picker.dismiss(animated: true, completion: nil)
	}

	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if picker == coverBackgroundImagePicker {
			guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
				return
			}
			
			delegate?.composeViewController(self, didSetCoverBackgroundImage: image)
		} else if picker == soundtrackVideoPicker {
			guard let mediaURL = info[UIImagePickerControllerMediaURL] as? URL else {
				return
			}
			
			delegate?.composeViewController(self, didSetSoundtrackVideo: AVURLAsset(url: mediaURL))
		}
		
		picker.dismiss(animated: true, completion: nil)
	}
}

extension ComposeViewController: UITextViewDelegate {
	
	func textViewDidBeginEditing(_ textView: UITextView) {
		let lightTextMenuItem = UIMenuItem(title: "Light text", action: #selector(ComposeViewController.setLightText))
		let darkTextMenuItem = UIMenuItem(title: "Dark text", action: #selector(ComposeViewController.setDarkText))
		
		let menuController = UIMenuController.shared
		menuController.menuItems = [lightTextMenuItem, darkTextMenuItem]
		menuController.update()
	}

	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		// If user entered a newline (by pressing return button, hopefully)...
		if text == "\n" {
			// ... dismiss the keyboard.
			textView.resignFirstResponder()
			return false
		} else {
			let changedText =
				String(NSString(string: textView.text ?? "")
					.replacingCharacters(in: range, with: text))
			changeCoverText(to: changedText)
			return true
		}
	}
}
